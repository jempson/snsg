#!/usr/bin/python3

"""
@author: 张伟
@time: 2018/2/16 10:52
"""
from setuptools import setup, find_packages

setup(
    name='snsg',
    version='1.8.8',
    author='tyoui',
    author_email='tyoui@tyoui.cn',
    description='this is a train text code',
    url='https://gitee.com/tyoui/snsg',
    download_url='https://gitee.com/tyoui/snsg.git',
    packages=find_packages(),
    platforms='any',
)
