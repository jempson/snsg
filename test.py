#!/usr/bin/python3

"""
@author: 张伟
@time: 2018/2/28 15:19
"""
import snsg

if __name__ == '__main__':
    s = snsg.SNSG(file_name="C:\\Users\\User\\Desktop\\1.txt")
    s.read()
    s.split()
    s.handle()
    data = s.filter()
    for key, value in data:
        print('关键字:' + key, '次数:' + str(value[0]), '频率:' + str(value[1]), '凝聚度:' + str(value[2]),
              '自由度:' + str(value[3]))
